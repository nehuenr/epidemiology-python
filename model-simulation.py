import matplotlib.pyplot as plt
import numpy
from random import seed
from random import random
import sys


def defineParameters (model):

  model = bestSuitedParameters (model)

  return model


def bestSuitedParameters (model):

  model ['parameters'] = {}
  model ['parameters']['N'] = 100                                 # population size
  model ['parameters']['epsilon'] = 0.00000001                    # min change exigency
  model ['parameters']['epsilon_check'] = 0                       # exigency fulfilled counter
  model ['parameters']['trajectory_T'] = [0]
  model ['parameters']['outbreak_shock'] = 0.05                   # infected population at t=0


  if model ['type'] == 'SIR':

    model ['parameters']['beta'] = 0.05                  # contagion rate
    model ['parameters']['gamma'] = 0.005                 # immunization rate
    model ['parameters']['min_T'] = 500
    model ['parameters']['max_T'] = 1200
    model ['parameters']['relevantVar'] = 'S'
    

  elif model ['type'] == 'SIRS':

    model ['typeDescription'] = 'with transitory imminuty\n'

    model ['parameters']['beta'] = 0.03                   # contagion rate
    model ['parameters']['gamma'] = 0.02                  # immunization rate
    model ['parameters']['delta'] = 0.001                 # immunity loss rate
    model ['parameters']['min_T'] = 1000
    model ['parameters']['max_T'] = 5000
    model ['parameters']['relevantVar'] = 'R'


  elif model ['type'] == 'SIRD':

    model ['typeDescription'] = 'with transitory immunity and decreasing N\n'

    model ['parameters']['beta'] = 0.03                   # contagion rate
    model ['parameters']['gamma'] = 0.02                  # immunization rate
    model ['parameters']['delta'] = 0.001                 # immunity loss rate
    model ['parameters']['ni'] = 0.00001                  # birth rate
    model ['parameters']['mu'] = 0.00005                  # death rate
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 17000
    model ['parameters']['relevantVar'] = 'I'


  elif model ['type'] == 'SIRDx':

    model ['typeDescription'] = 'with transitory immunity, constant N and higher mortality rate for I\n'

    model ['parameters']['beta'] = 0.03                   # contagion rate
    model ['parameters']['gamma'] = 0.01                  # immunization rate
    model ['parameters']['delta'] = 0.001                 # immunity loss rate
    model ['parameters']['ni'] = 0.0005                   # birth rate
    model ['parameters']['mu1'] = 0.0005                  # death rate for S and R population
    model ['parameters']['mu2'] = 0.001                   # death rate for I population
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 100000
    model ['parameters']['relevantVar'] = 'I'


  elif model ['type'] == 'SIRV':

    model ['typeDescription'] = 'with transitory immunity, constant N and vaccination\n'

    model ['parameters']['beta'] = 0.03                   # contagion rate
    model ['parameters']['gamma'] = 0.005                 # immunization rate
    model ['parameters']['delta'] = 0.01                  # immunity loss rate
    model ['parameters']['ni'] = 0.0001                   # birth rate
    model ['parameters']['mu1'] = 0.0001                  # death rate for S and R population
    model ['parameters']['mu2'] = 0.0001                  # death rate for I population
    model ['parameters']['vacc_rate'] = 0.0001            # vaccination rate
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 4000
    model ['parameters']['relevantVar'] = 'I'


  elif model ['type'] == 'SIRVx':

    model ['typeDescription'] = 'with transitory immunity, constant N and vaccination after date T\n'

    model ['parameters']['beta'] = 0.03                   # contagion rate
    model ['parameters']['gamma'] = 0.005                 # immunization rate
    model ['parameters']['delta'] = 0.01                  # immunity loss rate
    model ['parameters']['ni'] = 0.0001                   # birth rate
    model ['parameters']['mu1'] = 0.0001                  # death rate for S and R population
    model ['parameters']['mu2'] = 0.0001                  # death rate for I population
    model ['parameters']['vacc_rate'] = 0.00005           # vaccination rate
    model ['parameters']['vacc_T'] = 2000                 # vaccination start date
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 10000
    model ['parameters']['relevantVar'] = 'I'


  elif model ['type'] == 'SIRVxx':

    model ['typeDescription'] = 'with transitory immunity, growing N and vaccination after date T\n'

    model ['parameters']['beta'] = 0.03                   # contagion rate
    model ['parameters']['gamma'] = 0.005                 # immunization rate
    model ['parameters']['delta'] = 0.01                  # immunity loss rate
    model ['parameters']['ni'] = 0.00012                  # birth rate
    model ['parameters']['mu1'] = 0.0001                  # death rate for S and R population
    model ['parameters']['mu2'] = 0.0001                  # death rate for I population
    model ['parameters']['vacc_rate'] = 0.00005           # vaccination rate
    model ['parameters']['vacc_T'] = 2000                 # vaccination start date
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 10000
    model ['parameters']['relevantVar'] = 'I'


  elif model ['type'] == 'SIRVxxx':

    model ['typeDescription'] = 'with transitory immunity, constant N, higher mortality rate for I\n and vaccination after date T\n'

    model ['parameters']['beta'] = 0.03                   # contagion rate
    model ['parameters']['gamma'] = 0.005                 # immunization rate
    model ['parameters']['delta'] = 0.01                  # immunity loss rate
    model ['parameters']['ni'] = 0.0001                   # birth rate
    model ['parameters']['mu1'] = 0.0001                  # death rate for S and R population
    model ['parameters']['mu2'] = 0.0005                  # death rate for I population
    model ['parameters']['vacc_rate'] = 0.00005           # vaccination rate
    model ['parameters']['vacc_T'] = 2000                 # vaccination start date
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 10000
    model ['parameters']['relevantVar'] = 'I'


  elif model ['type'] == 'SIRVxxxx':

    model ['typeDescription'] = 'with transitory immunity, growing N, higher mortality rate for I\n and vaccination after date T\n'

    model ['parameters']['beta'] = 0.03                   # contagion rate
    model ['parameters']['gamma'] = 0.005                 # immunization rate
    model ['parameters']['delta'] = 0.01                  # immunity loss rate
    model ['parameters']['ni'] = 0.00013                  # birth rate
    model ['parameters']['mu1'] = 0.0001                  # death rate for S and R population
    model ['parameters']['mu2'] = 0.0005                  # death rate for I population
    model ['parameters']['vacc_rate'] = 0.00005           # vaccination rate
    model ['parameters']['vacc_T'] = 2000                 # vaccination start date
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 10000
    model ['parameters']['relevantVar'] = 'I'


  elif model ['type'] == 'SIRVT':

    model ['typeDescription'] = 'with transitory immunity, growing N, higher mortality rate for I,\n vaccination after date T and stochastic vaccination effectivy loss\n'

    model ['parameters']['beta'] = 0.02                   # contagion rate
    model ['parameters']['gamma'] = 0.005                 # immunization rate
    model ['parameters']['delta'] = 0.01                  # immunity loss rate
    model ['parameters']['ni'] = 0.00013                  # birth rate
    model ['parameters']['mu1'] = 0.0001                  # death rate for S and R population
    model ['parameters']['mu2'] = 0.0005                  # death rate for I population
    model ['parameters']['vacc_rate'] = 0.00005           # vaccination rate
    model ['parameters']['vacc_T'] = 1000                 # vaccination start date
    model ['parameters']['vacc_T_backup'] = 1000
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 50000
    model ['parameters']['relevantVar'] = 'I'


  elif model ['type'] == 'SIRVTx':

    model ['typeDescription'] = 'with transitory immunity, growing N, higher mortality rate for I,\n vaccination after date T, stochastic vaccination effectivy loss\n and growing contagious and mortality rate on every new variant\n'

    model ['parameters']['beta'] = 0.02                   # contagion rate
    model ['parameters']['gamma'] = 0.005                 # immunization rate
    model ['parameters']['delta'] = 0.01                  # immunity loss rate
    model ['parameters']['ni'] = 0.00013                  # birth rate
    model ['parameters']['mu1'] = 0.0001                  # death rate for S and R population
    model ['parameters']['mu2'] = 0.0005                  # death rate for I population
    model ['parameters']['vacc_rate'] = 0.00005           # vaccination rate
    model ['parameters']['vacc_T'] = 1000                 # vaccination start date
    model ['parameters']['vacc_T_backup'] = 1000
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 50000
    model ['parameters']['relevantVar'] = 'I'


  elif model ['type'] == 'SIRVTxx':

    model ['typeDescription'] = 'with transitory immunity, growing N, higher mortality rate for I,\n vaccination after date T, stochastic vaccination effectivy loss,\n growing contagious and mortality rate on every new variant and factible erradication of disease\n'

    model ['parameters']['beta'] = 0.02                   # contagion rate
    model ['parameters']['gamma'] = 0.005                 # immunization rate
    model ['parameters']['delta'] = 0.01                  # immunity loss rate
    model ['parameters']['ni'] = 0.00013                  # birth rate
    model ['parameters']['mu1'] = 0.0001                  # death rate for S and R population
    model ['parameters']['mu2'] = 0.0005                  # death rate for I population
    model ['parameters']['vacc_rate'] = 0.00005           # vaccination rate
    model ['parameters']['vacc_T'] = 1000                 # vaccination start date
    model ['parameters']['vacc_T_backup'] = 1000
    model ['parameters']['min_T'] = 0
    model ['parameters']['max_T'] = 10000
    model ['parameters']['relevantVar'] = 'I'


  return model


def defineSubtype (model):

  if model ['type'] in ['SIR']: model ['subtype'] = 'SIR'
  elif model ['type'][0:4] in ['SIRS', 'SIRD']: model ['subtype'] = 'SIR'
  elif model ['type'][0:4] in ['SIRV']: model ['subtype'] = 'SIRV'

  return model


def createModel (model_type):

  if model_type not in ['SIR', 'SIRS', 'SIRD', 'SIRDx', 'SIRV', 'SIRVx', 'SIRVxx', 'SIRVxxx', 'SIRVxxxx', 'SIRVT', 'SIRVTx', 'SIRVTxx']: return False

  model = {}

  model ['type'] = model_type
  model = defineSubtype (model)
  model = defineParameters (model)

  for var in split (model ['subtype']): model [var] = {}

  model ['S']['value'] = 1 - model ['parameters']['outbreak_shock']
  model ['I']['value'] = model ['parameters']['outbreak_shock']
  
  if 'R' in model: model ['R']['value'] = 0
  if 'V' in model: model ['V']['value'] = 0

  return model


def simulateTrajectory (model):

  while (abs (getDiff (model)) > model ['parameters']['epsilon'] and model ['parameters']['epsilon_check'] < 1) or (model ['parameters']['trajectory_T'][-1] > model ['parameters']['min_T']):

    if 'max_T' in model ['parameters']:
      
      if model ['parameters']['trajectory_T'][-1] > model ['parameters']['max_T']: break

    model = epsilonUpdate (model)

    model = storeTrajetory (model)

    model = simulateNextT (model)

    model = storeT (model)

  return model


def storeTrajetory (model):

  for var in split (model ['subtype']):

    if 'trajectory' not in model [var]:

      model [var]['trajectory'] = []

    model [var]['trajectory'].append (model [var]['value'])

  return model


def storeT (model):

  model ['parameters']['trajectory_T'].append (model ['parameters']['trajectory_T'][-1] + 1)

  return model


def simulateNextT (model):

  if 'beta' in model ['parameters']: beta = model ['parameters']['beta']
  if 'gamma' in model ['parameters']: gamma = model ['parameters']['gamma']
  if 'delta' in model ['parameters']: delta = model ['parameters']['delta']
  if 'ni' in model ['parameters']: ni = model ['parameters']['ni']
  if 'mu' in model ['parameters']: mu = model ['parameters']['mu']
  if 'mu1' in model ['parameters']: mu1 = model ['parameters']['mu1']
  if 'mu2' in model ['parameters']: mu2 = model ['parameters']['mu2']
  if 'vacc_rate' in model ['parameters']: vacc_rate = model ['parameters']['vacc_rate']


  v = varValues (model)
  N = countPopulation (model)
  T = model ['parameters']['trajectory_T'][-1]


  if model ['type'] == 'SIR':

    model ['S']['value'] += - beta * v ['I'] * v ['S']
    model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I']
    model ['R']['value'] += gamma * v ['I']


  elif model ['type'] == 'SIRS':

    model ['S']['value'] += - beta * v ['I'] * v ['S'] + delta * v ['R']
    model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I']
    model ['R']['value'] += gamma * v ['I'] - delta * v ['R']


  elif model ['type'] == 'SIRD':

    model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu * v ['S']
    model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu * v ['I']
    model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu * v ['R']


  elif model ['type'] == 'SIRDx':

    model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S']
    model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
    model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R']
  

  elif model ['type'] == 'SIRV':

    model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S'] - vacc_rate * T**0.5 * v ['S']
    model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
    model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R'] - vacc_rate * T**0.5 * v ['R']
    model ['V']['value'] += vacc_rate * T**0.5 * v ['S'] + vacc_rate * T**0.5 * v ['R'] - mu1 * v ['V']


  elif model ['type'] == 'SIRVx':

    if model ['parameters']['trajectory_T'][-1] < model ['parameters']['vacc_T']:

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R']
      model ['V']['value'] += 0

    else:

      vacc_T = model ['parameters']['vacc_T']

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S'] - vacc_rate * (T - vacc_T)**0.5 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R'] - vacc_rate * (T - vacc_T)**0.5 * v ['R']
      model ['V']['value'] += vacc_rate * (T - vacc_T)**0.5 * v ['S'] + vacc_rate * (T - vacc_T)**0.5 * v ['R'] - mu1 * v ['V']


  elif model ['type'] == 'SIRVxx':

    if model ['parameters']['trajectory_T'][-1] < model ['parameters']['vacc_T']:

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R']
      model ['V']['value'] += 0

    else:

      vacc_T = model ['parameters']['vacc_T']

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S'] - vacc_rate * (T - vacc_T)**0.5 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R'] - vacc_rate * (T - vacc_T)**0.5 * v ['R']
      model ['V']['value'] += vacc_rate * (T - vacc_T)**0.5 * v ['S'] + vacc_rate * (T - vacc_T)**0.5 * v ['R'] - mu1 * v ['V']


  elif model ['type'] == 'SIRVxxx':

    if model ['parameters']['trajectory_T'][-1] < model ['parameters']['vacc_T']:

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R']
      model ['V']['value'] += 0

    else:

      vacc_T = model ['parameters']['vacc_T']

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S'] - vacc_rate * (T - vacc_T)**0.5 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R'] - vacc_rate * (T - vacc_T)**0.5 * v ['R']
      model ['V']['value'] += vacc_rate * (T - vacc_T)**0.5 * v ['S'] + vacc_rate * (T - vacc_T)**0.5 * v ['R'] - mu1 * v ['V']


  elif model ['type'] == 'SIRVxxxx':

    if model ['parameters']['trajectory_T'][-1] < model ['parameters']['vacc_T']:

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R']
      model ['V']['value'] += 0

    else:

      vacc_T = model ['parameters']['vacc_T']

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S'] - vacc_rate * (T - vacc_T)**0.5 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R'] - vacc_rate * (T - vacc_T)**0.5 * v ['R']
      model ['V']['value'] += vacc_rate * (T - vacc_T)**0.5 * v ['S'] + vacc_rate * (T - vacc_T)**0.5 * v ['R'] - mu1 * v ['V']


  elif model ['type'] == 'SIRVT':

    if model ['parameters']['trajectory_T'][-1] < model ['parameters']['vacc_T']:

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R']
      model ['V']['value'] += 0

    else:

      vacc_T = model ['parameters']['vacc_T']
      vacc_T_backup = model ['parameters']['vacc_T_backup']

      if random () < 0.0002 and v ['I'] > 0:

        model ['parameters']['vacc_T'] = model ['parameters']['vacc_T_backup'] + T

        seed ('economic development rocks')

        model ['S']['value'] += + v ['V']
        model ['I']['value'] += 0
        model ['R']['value'] += 0
        model ['V']['value'] += - v ['V']

      else:

        model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S'] - vacc_rate * (T - vacc_T)**0.5 * v ['S']
        model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
        model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R'] - vacc_rate * (T - vacc_T)**0.5 * v ['R']
        model ['V']['value'] += vacc_rate * (T - vacc_T)**0.5 * v ['S'] + vacc_rate * (T - vacc_T)**0.5 * v ['R'] - mu1 * v ['V']


  elif model ['type'] == 'SIRVTx':

    if model ['parameters']['trajectory_T'][-1] < model ['parameters']['vacc_T']:

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R']
      model ['V']['value'] += 0

    else:

      vacc_T = model ['parameters']['vacc_T']
      vacc_T_backup = model ['parameters']['vacc_T_backup']

      if random () < 0.0002 and v ['I'] > 0:

        model ['parameters']['vacc_T'] = model ['parameters']['vacc_T_backup'] + T

        model ['parameters']['beta'] += 0.005
        model ['parameters']['mu2'] += 0.0001

        seed ('economic development rocks')

        model ['S']['value'] += + v ['V']
        model ['I']['value'] += 0
        model ['R']['value'] += 0
        model ['V']['value'] += - v ['V']

      else:

        model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S'] - vacc_rate * (T - vacc_T)**0.5 * v ['S']
        model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
        model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R'] - vacc_rate * (T - vacc_T)**0.5 * v ['R']
        model ['V']['value'] += vacc_rate * (T - vacc_T)**0.5 * v ['S'] + vacc_rate * (T - vacc_T)**0.5 * v ['R'] - mu1 * v ['V']


  elif model ['type'] == 'SIRVTxx':

    if model ['parameters']['trajectory_T'][-1] < model ['parameters']['vacc_T']:

      #if mu2 < mu1 + (ni-mu1)*(N/v['I']): print ('DOWN')
      #elif mu2 > mu1 + (ni-mu1)*(N/v['I']): print ('UP')

      model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S']
      model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
      model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R']
      model ['V']['value'] += 0

    else:

      #if mu2 < mu1 + (ni-mu1)*(N/v['I']): print ('DOWN')
      #elif mu2 > mu1 + (ni-mu1)*(N/v['I']): print ('UP')

      vacc_T = model ['parameters']['vacc_T']
      vacc_T_backup = model ['parameters']['vacc_T_backup']

      
      '''
      if v ['I'] < 0.05:

        v ['I'] = 0
        
        model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S'] - vacc_rate * (T - vacc_T)**0.5 * v ['S']
        model ['I']['value'] = v ['I']
        model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R'] - vacc_rate * (T - vacc_T)**0.5 * v ['R']
        model ['V']['value'] += vacc_rate * (T - vacc_T)**0.5 * v ['S'] + vacc_rate * (T - vacc_T)**0.5 * v ['R'] - mu1 * v ['V']
      '''

      if v ['I'] < 0.05 and v ['I'] > 0:
        
        model ['S']['value'] += 0
        model ['I']['value'] += - v ['I']
        model ['R']['value'] += v ['I']
        model ['V']['value'] += 0
      
        
      elif random () < 0.0001 and v ['I'] > 0:

        model ['parameters']['vacc_T'] = model ['parameters']['vacc_T_backup'] + T

        model ['parameters']['beta'] += 0.005
        model ['parameters']['mu2'] += 0.0001

        seed ('economic development rocks')

        model ['S']['value'] += + v ['V']
        model ['I']['value'] += 0
        model ['R']['value'] += 0
        model ['V']['value'] += - v ['V']
        

      else:

        model ['S']['value'] += ni * N - beta * v ['I'] * v ['S'] + delta * v ['R'] - mu1 * v ['S'] - vacc_rate * (T - vacc_T)**0.5 * v ['S']
        model ['I']['value'] += beta * v ['I'] * v ['S'] - gamma * v ['I'] - mu2 * v ['I']
        model ['R']['value'] += gamma * v ['I'] - delta * v ['R'] - mu1 * v ['R'] - vacc_rate * (T - vacc_T)**0.5 * v ['R']
        model ['V']['value'] += vacc_rate * (T - vacc_T)**0.5 * v ['S'] + vacc_rate * (T - vacc_T)**0.5 * v ['R'] - mu1 * v ['V']

  return model


def varValues (model):

  vV = {}

  for var in split (model ['subtype']):

    vV [var] = model [var]['trajectory'][-1]

  return vV 


def resizePopulation (model):

  for var in split (model ['subtype']):

    if 'trajectory' in model [var]:

      for i in range (0, len (model [var]['trajectory'])):

        model [var]['trajectory'][i] *= model ['parameters']['N']

  return model


def drawPopulation (model):

  populationTrajectory = []

  for t in model ['parameters']['trajectory_T'][0:-1]:

    N = 0

    for var in split (model ['subtype']):

      N += model [var]['trajectory'][t]


    populationTrajectory.append (N)

  model ['populationTrajectory'] = populationTrajectory

  return model


def countPopulation (model):

  N = 0

  for var in split (model ['subtype']):

    N += model [var]['trajectory'][-1]

  return N


def getDiff (model):

  var = model ['parameters']['relevantVar']

  if 'trajectory' in model [var]:

    if len (model [var]['trajectory']) > 1:
      
      return model [var]['trajectory'][-2] - model [var]['trajectory'][-1]

  return 2 * model ['parameters']['epsilon']


def epsilonUpdate (model):

  if abs (getDiff (model)) < model ['parameters']['epsilon']:
    model ['parameters']['epsilon_check'] += 1

  if abs (getDiff (model)) > model ['parameters']['epsilon']:
    model ['parameters']['epsilon_check'] = 0

  return model


def split (s): 
  return [char for char in s]  


def plot (model):

  trajectory_T = model ['parameters']['trajectory_T'][0:-1]

  w = 15
  h = 10
  d = 70
  plt.figure (figsize=(w, h), dpi=d)

  for var in split (model ['subtype']): 
    
    if 'trajectory' in model [var]:
      
      plt.plot (trajectory_T, model [var]['trajectory'])

  plt.plot (trajectory_T, model ['populationTrajectory'])

  if 'typeDescription' in model: plt.title ('SIR model ' + model ['typeDescription'])
  #if 'typeDescription' in model: plt.title ('SIRVT model')
  else: plt.title ('Basic SIR model')

  if model ['type'] in ['SIR']:
    a = numpy.array (split (model ['type']))

  elif model ['type'][0:4] in ['SIRS', 'SIRD']:
    a = numpy.array (split (model ['type'][0:3]))
    
  elif model ['type'][0:4] in ['SIRV']:
    a = numpy.array (split (model ['type'][0:4]))

  b = numpy.array (['N'])
  plt.legend (split (numpy.concatenate([a, b])))

  plt.xlabel ('Time')
  plt.ylabel ('Current values for each population group')

  plt.show ()


seed ('economic development rocks')

#model_type = 'SIR' # Basic SIR model
#model_type = 'SIRS' # SIR with transitory imminuty
#model_type = 'SIRD' # SIR with transitory immunity and decreasing population size
#model_type = 'SIRDx' # SIR with transitory immunity, constant population size and higher mortality rate for infected population
#model_type = 'SIRV' # SIR with transitory immunity, constant population size and vaccination
#model_type = 'SIRVx' # SIR with transitory immunity, constant population size and vaccination after date T
#model_type = 'SIRVxx' # SIR with transitory immunity, growing population size and vaccination after date T
#model_type = 'SIRVxxx' # SIR with transitory immunity, constant population size, higher mortality rate for infected population and vaccination after date T
#model_type = 'SIRVxxxx' # SIR with transitory immunity, growing population size, higher mortality rate for infected population and vaccination after date T
#model_type = 'SIRVT' # SIR with transitory immunity, growing population size, higher mortality rate for infected population, vaccination after date T and stochastic vaccination effectivy loss
#model_type = 'SIRVTx' # SIR with transitory immunity, growing population size, higher mortality rate for infected population, vaccination after date T, stochastic vaccination effectivy loss and growing contagious and mortality rate on every new variant
model_type = 'SIRVTxx' # SIR with transitory immunity, growing N, higher mortality rate for infected, vaccination after date T, stochastic vaccination effectivy loss, growing contagious and mortality rate on every new variant and factible erradication of disease




model = createModel (model_type)

model = simulateTrajectory (model)

model = resizePopulation (model)

model = drawPopulation (model)

plot (model)